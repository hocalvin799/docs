# Release Manager

The release manager drives the [monthly release] of GitLab, any
[patch releases] and [security releases] for that version.

## Responsibilities

You as a release manager have a responsibility to deliver the work of every
single person involved in creating the product and running the application at
GitLab.com.

This responsibility sometimes requires making difficult decisions. For example,
you might need to refuse including a feature or a change within a release.
You might need to decide on shipping a feature that is not working as expected
to allow for other features/fixes to be available. You might need to revert
someone's work because it was impacting the release schedule.

The decisions you make can have a cascading effect on all other release tasks,
so make sure that you collect as much data as you can within the time you have,
make an informed decision, and stick with it.

At all times, keep in mind that protecting GitLab users and the stability of
GitLab.com are more important than accepting a change in release to please
someone from you or another team.

When you start as a release manager, some of your responsibilities are:

* Escalating to responsible parties in case a release task is slowed down or
  blocked by their area of responsibility
* Ensuring that the automated QA is successfully completed
* Monitoring our auto-deploy process
* Deploying to Production
* Understanding the impact of the deployment on environment
* Communicating with the public through writing release blog posts
* [Handling deployment failures](../general/deploy/failures.md)

The monthly self-managed releases are a company-wide effort, and should not
fall entirely on the release manager's shoulders. More about that in the sections
that follow.

Just one final thing before you get started: keep calm and move fast!

## Getting Started

If you are assigned to the release management duty, the first course of action
should be:

1. To [onboard](#onboarding) and,
2. To create a [monthly release issue].

With access to tools required to operate the releases, you can start with your
first task.

### Onboarding

Create a [new issue in the **release/tasks** project][new issue] and select the
`RM-Onboarding` template. Use the title `Onboarding Release Manager YOUR_NAME_HERE`.

Assign the issue to the [previous Release Manager][managers] in your timezone.

The tasks are ordered by priority and should be completed before starting your
appointed release.

### Training

Now is a good time to talk with the previous Release Managers. They should
be able to answer any question you have. If they don't know the answer, they should
direct you to a person who might know. Feel free to go as far as necessary to
get your answer. Do remember to document when you find that answer!

### Release Candidates

[Release Candidate (RC)][release-candidates] is a point in time snapshot of what will become a release.
Any RC that gets created can be considered for final release.

### Deployment

The release manager is also responsible for promoting the latest versions through GitLab.com environments.

### QA task

Quality assurance (QA) is how we reduce the possibility of shipping a broken
feature. We have two types of QA, automated and manual.

Automated QA is ran with deployment pipelines.
Manual QA is in a form of an issue with a number of checkboxes that engineers
need to go through.

Your responsibility is to monitor the automated QA for successful runs
and the issue in case something is reported there. As engineers check off items
they may raise a concern in this issue and together with them it is necessary to
assign a correct priority.

Find out more about QA task in a separate [qa documentation][qa].

### Post-deployment Patch

Post-deployment patches are something we manage the lifecycle of.  It would be
wise to read up and get to know the process as outlined in [the documentation](https://gitlab.com/gitlab-org/release/docs/blob/master/general/deploy/post-deployment-patches.md)

### Patch Release

As the final self-managed release gets published to users on the 22nd, it is
expected to have a number of patch releases to resolve any outstanding bugs
reported after the release.

The amount and scheduling of [patch releases][patch] is entirely at your discretion as
the release manager.

Exception is reserved for [critical security releases](../general/security/process.md#critical-security-releases), which should be
addressed immediately. [Security process](../general/security/process.md)
diverges from this process.

If a bug affects a large number of users and/or a critical piece of
functionality, it's fine to release a patch with only one fix. Sometimes a patch
will include five or more minor fixes. You should use your
best judgment to determine when a patch release is warranted. If you are not sure,
you can always ask for help on deciding at #releases. We strive to
continue releasing patches until all (or most) known regressions for that release are
addressed.

To help you understand what is expected from you when doing patch releases,
check out [Patch release documentation][patch].

## Offboarding

Once there's no immediate action to create a patch release, Release Managers
duties have finished. To offboard archive the old release channel `#f_release_x_x`.

[monthly release]: ../general/monthly.md
[patch releases]: ../general/patch/process.md
[security releases]: ../security/process.md
[release-candidates]: ../general/release-candidates.md
[deployment]: https://gitlab.com/gitlab-org/takeoff#deploying-gitlab
[qa]: ../general/qa-checklist.md
[patch]: ../general/patch/process.md
[new issue]: https://gitlab.com/gitlab-org/release/tasks/issues/new
[managers]: https://about.gitlab.com/release-managers/
[monthly release issue]: ../general/monthly.md/release-manager.md#create-an-issue-to-track-the-release

---

[Return to Quick Start](../README.md#quick-start)
