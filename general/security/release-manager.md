# Security Releases as a Release Manager

## General process overview

As noted in the [general process document](process.md), release managers
are responsible for handling a [critical security release](#critical) or a
[non-critical security release](#non-critical).

Before reading the specifics for each, please read through the general
information below which applies to both.

### What to include

A security release, even one for the latest monthly release, should _only_
include the changes necessary to resolve the security vulnerabilities. Including
fixes for regressions in a security patch increases the chances of breaking
something, both for users and for our packaging and release process.

The only exception to this policy is [release candidates]. If the monthly
release is in progress as we're preparing for a security release, it's
acceptable for a new RC to include both security fixes and regression fixes.
Care should be taken to coordinate the publishing of an RC package with the
other security patches so as to not disclose the security vulnerabilities
publicly before we're ready to disclose them.

[release candidates]: ../release-candidates.md

### Process

When working on a security release, it's important that all work happens
**only** on [dev.gitlab.org], so nothing is disclosed publicly before we intend
it.

You should run `scripts/security-harness` in the CE, EE, and Omnibus projects in
order to prevent accidental pushes to any remote other than `dev.gitlab.org`.

We normally try to avoid releasing security packages to the community on Friday
in order to avoid situations where a customer may be exploited during the
weekend.

[dev.gitlab.org]: https://dev.gitlab.org/

### Security branches

Developers [work off of security branches](developer.md#branches). As the
release manager, you'll be merging fixes straight into the stable branches
whenever a fix is ready.

## Critical

From a release manager standpoint, a critical security release requires drafting
a release plan with a timeline in addition to tasks defined for [non-critical
security release](#non-critical).

### Creating a release plan

When a [Security Engineer] initiates a critical security release, release
managers' first responsibility is creating a release timeline.

It is important to note that "critical" does not mean that the release needs to
happen the very moment it's requested. There might be multiple release tasks
that are currently executed, so it is important not to panic.

A few questions that you can discuss with your fellow release managers:

* Are the fixes ready for issue(s) that prompted this security release?
  * If no, find out when the fixes will be ready for the issue and *all backport* versions.
  * If yes, are all backports ready? If yes, this means that release managers have everything they need to work on the release.
* Where are we currently in the release cycle?
  * If you are working on patch releases:
    * If the release is not tagged, consider postponing the patch release.
    * If the release is tagged, complete the patch release before proceeding further.
  * If you are working on the latest monthly release:
    * Will working on security release endanger the monthly release? Is it
      possible to work on RCs and all backports for the security release without
      breaching the deadline?
    * If you prepare a security release and it gets postponed for some reason,
      will creating all other RCs as security releases cause more work?

When you get answers for the questions above, start working on the timeline for
the critical security release.

An example of how a release timeline could look like, when a security release
is called for during the latest monthly release:

```
To meet the deadline of 22nd and not block the regular release, proposing a schedule:

* All fixes and backports need to be ready for merge by the end of Monday.
* @release-manager-1 will tag, deploy the RC to staging, and create a QA issue by the end of their Monday.
* @release-manager-2 will merge all security fixes into their respective branches at the start of their Tuesday.
* @release-manager-2 will tag the release and hand over to Quality and Security for QA tasks, which need to be completed by
end of day Tuesday.
* @release-manager-1 will deploy the RC to production at the start of their Tuesday.
* @release-manager-2 will be ready to promote the release when QA tasks are ready at the start of their Wednesday.
```

In this example, `@release-manager-1` and `@release-manager-2` are in different timezones (EMEA/US).
The plan can be more detailed to include more people with specific tasks.
Aim to `@`-mention people responsible for specific tasks, to avoid the
[bystander effect](https://en.wikipedia.org/wiki/Bystander_effect).

Once the plan is presented in the release issue and related channels, it is important to
stick with the plan. You should refuse any changes with the items that need to be included
in the release if they put the release deadlines at risk.
You can consider being flexible if there is enough time to recover from failure
(CI failing, deploy goes wrong, etc.),
but in most cases you will need to stick strictly to the plan to make sure that the
deadlines given to the Security team are respected.
If you are not sure if your plan will work, ask the Release Coordinator to
help with advice.

Once the plan is defined, you can continue as with
[non-critical security release](#non-critical).

## Non-critical

From a release manager standpoint, a non-critical security release is fairly
similar to a [patch release](../patch.md), but with a few extra considerations
in order to avoid disclosing vulnerabilities before proper fixes are released.
Additional collaboration is needed to coordinate with the Security and Marketing
teams.

## General steps

1. Security Release Issue

    [Security Engineer] opens the confidential issue for security releases on gitlab.com.
    This is a meta issue, each version to be released will have its own issue.

1. Merge Request that need to be picked

    The Release Manager can use the `security` label to filter the MRs that are
    ready to be merged for the next security release. Only merge requests that
    have this label and are assigned to @gitlab-release-tools-bot can be merged.

    For each scheduled issue, the Release Manager should merge at least four
    merge requests: one for `master`, one for the current release and one for
    each of the two latest releases. More, if there are EE MRs.

    *Example:* If the current milestone is 10.8 you should open an MR targeting
    10.8, 10.7 and 10.6 security branches, as well as one targeting `master`.

1. List the issues and merge requests that need to be included

    For each Merge Request that the Release Manager was assigned to, list the issue and each merge request in the security issue.
    It will be helpful when it's time to merge them.

1. Merge the MRs assigned to the GitLab Release Tools Bot targeting `X-Y-stable[-ee]` branches

    It is the Release Manager responsibility to merge the MRs targeting the last 3 releases. **Do not merge the one targeting master yet**

    If MR has red pipelines, reassign all related MR's to the original author. Inform the point of contact from the security team for the current release
    by leaving a comment in the release issue. Edit the issue description to state that the issue is excluded from the release due to failing pipelines.

1. Create Security Patch Issues in [gitlab-org/release/tasks](https://gitlab.com/gitlab-org/release/tasks)
  using the [`prepare:security` task][prepare-security-task-doc].

1. Link the new issues as related issues to the meta release issue.

1. Follow the steps on the template *for each* of the created issues.

1. Merge MRs targeting `master` should be done last

    After promoting the packages to public, the Release Manager should merge the MR targeting `master`. See more information about this step in [developer guide](developer.md#final-steps)

## Procedure

1. Generate the release issue
    * `/chatops run release prepare --security`
    * This will automatically create 3 issues targeting the latest 3 versions of
      GitLab
1. Follow guidelines in each issue to proceed - stopping at the `Release`
   section
1. Prior to releasing, we need to get the auto-deploy setup with our fixes,
   continue this below

### Auto-deploy Steps

The following steps should ideally be completed w/i 1 day

1. Notify #development that auto-deploy will be disabled until the security release is out
1. Ensure latest auto-deploy branches are sync'd across .com and dev
1. Disable the `Auto-deploy MR picker` job
1. Disable the `Auto-deploy tagging` job
1. Disable the `Auto-deploy branch creation` job
1. `/chatops run release merge --security --master`
1. manually cherry pick EACH security fix into the CURRENT auto-deploy branch
1. Modify the `Auto-deploy tagging job` - add ENV variable `SECURITY` set to `true`
1. Manually kick off Auto-deploy tagging job
    * This will automatically deploy to staging
    * If successful this will also create a QA issue
    * Ensure this makes it through to canary
1. Modify the Auto-deploy tagging job - remove ENV variable SECURITY
1. After the QA deadline has passed, proceed to push this to production

The below is able to be accomplished on the next working day

1. Pick up where we left off in the Release Section of the 3 issues that were
   created from above
1. Enable the `Auto-deploy MR picker` job
1. Enable the `Auto-deploy tagging` job
1. Enable the `Auto-deploy branch creation` job
1. Manually kick off creating a new auto-deploy branch
    * This will bring auto-deploy back online and contain our security fixes plus
      any new merge requests since auto-deploy was paused.

---

[Return to Security Guide](process.md)

[Security Engineer]: security-engineer.md
[prepare-security-task-doc]: https://gitlab.com/gitlab-org/release-tools/blob/master/doc/rake-tasks.md#preparesecurity
