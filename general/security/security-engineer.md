# Security engineer in security releases

There are two types of security releases: [critical](#critical) and
[non-critical](#non-critical). The main difference is in who initiates the
release process. Critical security releases are initiated on-demand by the
security team to resolve the most severe vulnerabilities that are labelled as
`S1`. They are highly urgent and need to be released as soon as the patch is
ready and it is possible to perform a release.

Non-critical security releases are scheduled monthly and are initiated
by the release management team. These contain non P1 and S1 issues.

### Non-critical

The Security Engineer is responsible for opening a new security release issue
after the completion of the previous non-critical security release. Identify
the next release managers from the [release managers page](https://about.gitlab.com/release-managers/)
and assign the issue to them.

1. For each provided fix for a security issue:
    1. Request a pre-assigned CVE using the [webform][cve].
    1. Add it to the next security release issue so that it can be backported and released.
1. Set the previous month's regular security release to public by following the [post release steps](https://gitlab.com/gitlab-org/release/docs/blob/master/general/security/security-engineer.md#post-release) for it.
1. *Before 23:59 PT on the 24th:* start working on [preparing the blog post](#prepare-blog-post).
1. Next, follow the process for [verifying fixes](#verify-fixes).
1. When the release is ready for publishing, follow steps in [finalize release](#finalize-release).
1. Create a new issue for the next scheduled security release and begin adding issues which are ready to be backported and released.  This may need to happen earlier to track fixes that did not meet the deadline. Document this new release issue in the title of the #releases Slack channel.

### Critical

Once a fix has been provided for a critical `S1` labelled security issue:

1. Request a pre-assigned CVE using the [webform][cve].
1. Create a critical security release issue to be able to coordinate the release easier.
1. Notify release managers of a need to prepare for the critical security
   release. If the fix is not ready yet, provide release managers with an
   estimate of when the fix is going to be ready so that they can create a
   release plan.
1. Once release managers have a proposed plan, start [preparing the blog post](#prepare-blog-post).
1. Next, follow the process for [verifying fixes](#verify-fixes).
1. When the release is ready for publishing, follow steps in [finalize release](#finalize-release).
1. 30 days after the release is published, follow steps in [Post Release](#post-release).

## General process

### Prepare blog post

* Create a WIP blog post MR in dev.gitlab.org. Ensure to add the CVE IDs, vulnerability descriptions, affected versions, remediation info, and a thank you to the reporter. Much of this information can be copy and pasted from the [summary table](https://dev.gitlab.org/gitlab/gitlabhq/blob/master/.gitlab/issue_templates/Security%20Developer%20Workflow.md#summary) in the developer security issue created on [GitLab Security]. Also, feel free to use a past security release as a guide, for example [this blog post](https://about.gitlab.com/2018/04/04/security-release-gitlab-10-dot-6-dot-3-released/).
* Please add `/source/images/blogimages/security-cover-new.png` to the `image_title:` field in the front matter.
* Please include a "Receive Security Release Notifications" section at the very bottom of the blog post with links to our contact us page and RSS feed. See previous security release posts or [this issue](https://gitlab.com/gitlab-com/gl-security/security-communications/communications/issues/145#note_240450797) for examples.
* Assign the blog post to the RM for review.
* Create a WIP issue using the `Security Release Email Alert` template in https://gitlab.com/gitlab-com/gl-security/security-communications/communications/issues and request an email notification be sent to subscribers of the `Security Alert` segment on the day of the release. Include a note that this should be sent out **after** the blog post is live. Also mention that you'll include the link to the blog post MR once it is prepared. The content of this notification should be similar to the blog post introduction:

>"Today we are releasing versions X.X.X, X.X.X, and X.X.X for GitLab Community Edition (CE) and Enterprise Edition (EE).

>These versions contain a number of important security fixes, and we strongly recommend that all GitLab installations be upgraded to one of these versions immediately. 

>Please forward this alert to appropriate people at your organization and have them subscribe to [Security Notices](https://about.gitlab.com/contact/). You can also receive security release blog updates by subscribing to our [RSS feed](https://about.gitlab.com/security-releases.xml).

>For more details about this release, please visit our [blog](TODO)."

### Verify fixes

Quality Engineer notifies Security Engineer that the testing environments are ready for testing. Once the `QA issue` is created, Security Engineer, needs to:

* Assign yourself to the issue. This specifies that you are working on verifying the fix.
* Validate the fixes. Testing can be performed on either:
  * The `pre.gitlab.com` environment.
  * Locally via the docker images of the built packages at https://dev.gitlab.org/gitlab/omnibus-gitlab/container_registry
    * i.e. `docker pull dev.gitlab.org:5005/gitlab/omnibus-gitlab/gitlab-ee:X.X.X-ee.0`

* Verify fixes on all backports on environments provided by Quality.
* Once validated, assign the issue back to the release manager(s) and notify them that the fixes are ready to be published.

### Finalize release

Once blog post on dev.gitlab.org has been reviewed and all packages are ready for
publishing:

* Release manager starts publishing the packages
* Pull the latest changes from the `www-gitlab-com` repository on GitLab.com into master branch
* Merge the dev.gitlab.org MR into the master branch
* Pull the changes from dev.gitlab.org to your local machine
* Sync the two remotes by pushing the master branch to both GitLab.com and dev.gitlab.org
* Put the link of the new blog post in the email notification request issue as well as the security release issue
* Make sure the CVE IDs are documented in the corresponding GitLab.com issues and H1 reports
* Ping in H1 reports the appsec team member who triaged the issue to notify a fix has been released
* Close out the issues on both `gitlab-org/gitlab` and `gitlab-org/security/gitlab`.

### Post release

30 days after release
 * Remove `confidential` from issues unless they have been labelled `~keep confidential`.
 * Update the CVE IDs to public. See `Notify CVE about a publication` on the [webform](https://cveform.mitre.org/).

[cve]: https://cveform.mitre.org/
[GitLab Security]: https://gitlab.com/gitlab-org/security/gitlab
