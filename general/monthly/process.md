# Monthly release general process

The release process follows [this set of templates](https://gitlab.com/gitlab-org/release-tools/tree/master/templates)

## First RC (RC1)
* Automated tests are run against Staging.
* A QA Issue is automatically created listing the changes for the release candidate
  * Changes are displayed as Merge Requests and are collected via a comparison of this RC against the previous stable release, associated with the Merge Request author.
  * The Engineering teams confirms that these other changes come with tests or have been tested on staging. 
  * Regressions and bugs are raised and resolved.
  * Example: [11.6.0-rc1 QA Issue](https://gitlab.com/gitlab-org/release/tasks/issues/576)
* After 24 hours, assuming there are no blocking bugs found, the expectation is testing has completed and the release can proceed to deploy to production.

## Subsequent RCs
* Automated tests are run against Staging.
* A QA issue is automatically created listing the changes for the release candidate
  * The Merge Requests are merged since the previous release candidate are captured in the QA issue, associated with the Merge Request author.
  * The engineer performs testing on staging, or confirms that the change came with automated tests that are passing.
  * Regressions and bugs are raised and resolved.
  * Example: [11.6.0-rc4 QA Issue](https://gitlab.com/gitlab-org/release/tasks/issues/589)
* After 12 hours, assuming there are no blocking bugs found, the expectation is testing has completed and the release can proceed to deploy to production.

## Final Release
* The process is the same as the **Subsequent RCs**.
* Automated tests are run against Canary as a sanity check.
* The final release candidate is deployed to production.

# Monthly Process as a Diagram
```mermaid
gantt
  title Release Process
  dateFormat  YYYY-MM-DD

  section Branch Sync
  merge-train CE => EE              :2018-01-22, 33d
  manual master => stable-11-8[-ee] :2018-01-22, 2018-02-07

  section Development
  11.8 Feature Development :2018-01-22, 2018-02-07
  11.8 Feature Freeze      :crit, 2018-02-07, 2018-02-22
  11.9 Feature Development :2018-02-07, 2018-02-24

  section Monthly Task
  11-8 issue                    :a1, 2018-01-22, 33d
  create 11-8-0 rc1             :a2, 2018-02-07, 2h
  tag 11-8-0 rc1                :a3, after a2,   2h
  qa 11-8-0 rc1                 :a4, after a3,   24h
  deploy canary 11-8-0 rc1      :a5, after a4,   3h
  deploy production  11-8-0 rc1 :a6, after a5,   3h
  publish 11-8-0 rc1            :a7, after a6,   1h

  create 11-8-0 rc2             :b1, after a6,   1h
  tag 11-8-0 rc2                :b2, after b1,   2h
  qa 11-8-0 rc2                 :b3, after b2,   24h
  deploy canary 11-8-0 rc2      :b4, after b3,   3h
  deploy production  11-8-0 rc2 :b5, after b4,   3h
  publish 11-8-0 rc2            :b6, after b5,   1h

  create 11-8-0 rc3             :c1, after b6,   1h
  tag 11-8-0 rc3                :c2, after c1,   2h
  qa 11-8-0 rc3                 :c3, after c2,   24h
  deploy canary 11-8-0 rc3      :c4, after c3,   3h
  deploy production  11-8-0 rc3 :c5, after c4,   3h
  publish 11-8-0 rc3            :c6, after c5,   1h

  create 11-8-0 rc4             :d1, after c6,   1h
  tag 11-8-0 rc4                :d2, after d1,   2h
  qa 11-8-0 rc4                 :d3, after d2,   24h
  deploy canary 11-8-0 rc4      :d4, after d3,   3h
  deploy production  11-8-0 rc4 :d5, after d4,   3h
  publish 11-8-0 rc4            :d6, after d5,   1h

  create 11-8-0 rc5             :e1, after d6,   1h
  tag 11-8-0 rc5                :e2, after e1,   2h
  qa 11-8-0 rc5                 :e3, after e2,   24h
  deploy canary 11-8-0 rc5      :e4, after e3,   3h
  deploy production  11-8-0 rc5 :e5, after e4,   3h
  publish 11-8-0 rc5            :e6, after e5,   1h

  create 11-8-0 rc6             :f1, after e6,   1h
  tag 11-8-0 rc6                :f2, after f1,   2h
  qa 11-8-0 rc6                 :f3, after f2,   24h
  deploy canary 11-8-0 rc6      :f4, after f3,   3h
  deploy production  11-8-0 rc6 :f5, after f4,   3h
  publish 11-8-0 rc6            :f6, after f5,   1h

  create 11-8-0 rc7             :g1, after f6,   1h
  tag 11-8-0 rc7                :g2, after g1,   2h
  qa 11-8-0 rc7                 :g3, after g2,   24h
  deploy canary 11-8-0 rc7      :g4, after g3,   3h
  deploy production  11-8-0 rc7 :g5, after g4,   3h
  publish 11-8-0 rc7            :g6, after g5,   1h

  create 11-8-0 rc8             :h1, after g6,   1h
  tag 11-8-0 rc8                :h2, after h1,   2h
  qa 11-8-0 rc8                 :h3, after h2,   24h
  deploy canary 11-8-0 rc8      :h4, after h3,   3h
  deploy production  11-8-0 rc8 :h5, after h4,   3h
  publish 11-8-0 rc8            :h6, after h5,   1h

  create 11-8-0 rc9             :i1, after h6,   1h
  tag 11-8-0 rc9                :i2, after i1,   2h
  qa 11-8-0 rc9                 :i3, after i2,   24h
  deploy canary 11-8-0 rc9      :i4, after i3,   3h
  deploy production  11-8-0 rc9 :i5, after i4,   3h
  publish 11-8-0 rc9            :i6, after i5,   1h

  create 11-8-0 rc10             :j1, after i6,   1h
  tag 11-8-0 rc10                :j2, after j1,   2h
  qa 11-8-0 rc10                 :j3, after j2,   24h
  deploy canary 11-8-0 rc10      :j4, after j3,   3h
  deploy production  11-8-0 rc10 :j5, after j4,   3h
  publish 11-8-0 rc10            :j6, after j5,   1h

  create 11-8-0 rc11             :k1, after j6,   1h
  tag 11-8-0 rc11                :k2, after k1,   2h
  qa 11-8-0 rc11                 :k3, after k2,   12h
  deploy canary 11-8-0 rc11      :k4, after k3,   3h
  deploy production  11-8-0 rc11 :k5, after k4,   3h
  publish 11-8-0 rc11            :k6, after k5,   1h

  create 11-8-0            :m1, 2018-02-22, 1h
  tag 11-8-0               :m2, after m1,   2h
  deploy canary 11-8-0     :m3, after m2,   3h
  deploy production 11-8-0 :m4, after m3,   3h
  publish 11-8-0           :m5, after m4,   1h
  tweet 11-8-0             :m6, after m4,   1h
  blog 11-8-0              :m7, after m4,   1h
```

## Process

* [Monthly Issue](https://gitlab.com/gitlab-org/release/docs/blob/master/general/monthly.md)
* [Release Candidates](https://gitlab.com/gitlab-org/release/docs/blob/master/general/release-candidates.md)
* [Prepare Merge Requests](https://gitlab.com/gitlab-org/release/docs/blob/master/general/picking-into-merge-requests.md)
* [Cherry Picking](https://gitlab.com/gitlab-org/release/docs/blob/master/general/picking-into-merge-requests.md)
* [Tagging and Releasing](https://gitlab.com/gitlab-org/release/docs/blob/master/general/publishing-packages.md)
* [Deployment Documentation](../deploy/gitlab-com-deployer.md#CI_Pipeline_for_Deployment)

## Policy
* [Versioning Policy](https://docs.gitlab.com/ee/policy/maintenance.html)
